import { Router } from 'express';
import { getUserNotes, addUserNotes, getUserNoteById, updateUserNoteById, toggleCompletedForUserNoteById, deleteUserNoteById } from '../controllers/notes-controller.js';
import checkAuth from '../middlewares/auth.js';

const router = Router();

router.use(checkAuth);

router.get('/', getUserNotes);

router.post('/', addUserNotes);

router.get('/:noteId', getUserNoteById);

router.put('/:noteId', updateUserNoteById);

router.patch('/:noteId', toggleCompletedForUserNoteById);

router.delete('/:noteId', deleteUserNoteById);

export default router;