import { Router } from 'express';
import { createProfile, login } from '../controllers/auth-controller.js';

const router = Router();

router.post('/register', createProfile);

router.post('/login', login);

export default router;