import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './auth/auth.guard';
import { NoteEditComponent } from './notes/note-edit/note-edit.component';
import { NotesComponent } from './notes/notes.component';

const routes: Routes = [
    { path: '', component: NotesComponent, canActivate: [AuthGuard] },
    { path: 'auth', component: AuthComponent },
    { path: 'notes/:noteId/update', component: NoteEditComponent, canActivate: [AuthGuard] },
    { path: 'notes/add', component: NoteEditComponent, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
