import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, throwError } from 'rxjs';

import { catchError, tap } from 'rxjs/operators';
import { User } from './user.model';

export interface GetUserProfileResponse {
    user: {
        _id: string;
        username: string;
        createdDate: string;
    }
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    user = new BehaviorSubject<User | null>(null);
    token = new BehaviorSubject<string | null>(null);

    constructor(private http: HttpClient, private router: Router) { }

    register(username: string, password: string) {
        return this.http.post(`http://localhost:8080/api/auth/register`, {
            username,
            password
        })
            .pipe(catchError(errorRes => throwError(() => new Error(errorRes?.error?.message))));
    }

    login(username: string, password: string) {
        return this.http.post<{ message: string, jwt_token: string }>(`http://localhost:8080/api/auth/login`, {
            username,
            password
        })
            .pipe(
                catchError(errorRes => throwError(() => new Error(errorRes?.error?.message))),
                tap(responseData => {
                    this.token.next(responseData.jwt_token);
                    localStorage.setItem('token', responseData.jwt_token);
                })
            );
    }

    getUserProfile() {
        return this.http.get<GetUserProfileResponse>(`http://localhost:8080/api/users/me`)
            .pipe(
                tap(responseData => {
                    localStorage.setItem('userData', JSON.stringify({
                        'username': responseData.user.username,
                        'createdDate': responseData.user.createdDate
                    }));
                    this.user.next(responseData.user);
                })
            )
    }

    autoLogin() {
        const user = JSON.parse(localStorage.getItem('userData')!!);
        const token = localStorage.getItem('token');

        if (!user || !token) {
            return;
        }

        const loadedUser = new User(
            user.username,
            user.createdDate
        );

        this.token.next(token);
        this.user.next(loadedUser);
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('userData');
        this.user.next(null);
        this.token.next(null);
        this.router.navigate(['/auth']);
    }
}
