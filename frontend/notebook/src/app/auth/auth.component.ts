import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
    isLoginMode = false;
    authForm!: FormGroup;
    error: string = '';
    formSubmitting = false;
    registerMessage = '';

    constructor(private authService: AuthService, private router: Router) { }

    ngOnInit(): void {
        this.authForm = new FormGroup({
            'username': new FormControl(null, [Validators.required]),
            'password': new FormControl(null, [Validators.required])
        });
    }

    onSwitchMode() {
        this.registerMessage = '';
        this.isLoginMode = !this.isLoginMode;
    }

    onSubmit() {
        if (!this.authForm.valid) {
            return;
        }

        this.formSubmitting = true;

        const { username, password } = this.authForm.value;
        let authObservable: Observable<any>;

        if (this.isLoginMode) {
            authObservable = this.authService.login(username, password);
        } else {
            authObservable = this.authService.register(username, password);
        }

        authObservable.subscribe({
            next: (responseData) => {
                this.formSubmitting = false;
                if (!responseData.jwt_token) {
                    this.registerMessage = responseData.message;
                    return;
                }
                this.authService.getUserProfile().subscribe(_ => {
                    this.authForm.reset();
                    this.router.navigate(['/']);
                });
            },
            error: (errorMessage: string) => {
                this.formSubmitting = false;
                this.error = errorMessage;
            }
        });
    }
}
