import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Note } from './note.model';
import { GetNotesResponse, NotesService } from './notes.service';

@Component({
    selector: 'app-notes',
    templateUrl: './notes.component.html',
    styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
    notes: Note[] = [];
    loadingNotes = false;
    loadingMoreNotes = false;
    limit = 3;
    offset = 0;
    count = 0;

    constructor(private authService: AuthService, private notesService: NotesService) { }

    ngOnInit(): void {
        this.authService.autoLogin();

        this.loadingNotes = true;

        this.notesService.getUserNotes(0).subscribe({
            next: (responseData) => {
                this.handleNotes(responseData, false);
            }
        });
    }

    onNoteChanged(noteId: string) {
        const note = this.notes.find(note => note._id === noteId);
        note!.completed = !note!.completed;
    }

    loadMoreNotes() {
        this.loadingMoreNotes = true;

        this.notesService.getUserNotes(this.offset).subscribe({
            next: (responseData) => { 
                this.handleNotes(responseData, true);
            }
        })
    }

    onNoteDeleted() {
        this.loadingNotes = true;

        this.notesService.getUserNotes(0).subscribe({
            next: (responseData) => {
                this.offset = 0;
                this.handleNotes(responseData, false);
            }
        })
    }

    private handleNotes(responseData: GetNotesResponse, loadMore: boolean) {
        if (!loadMore) {
            this.loadingNotes = false;
        } else {
            this.loadingMoreNotes = false;
        }
        this.count = +responseData.count;
        this.offset += +responseData.limit;
        this.notes = loadMore ? [...this.notes, ...responseData.notes] : responseData.notes;
    }
}
