export interface Note {
    _id: string;
    text: string;
    completed: boolean;
    createdDate: string;
}