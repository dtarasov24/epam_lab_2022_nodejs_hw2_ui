import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Note } from '../note.model';
import { NotesService } from '../notes.service';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-note-edit',
    templateUrl: './note-edit.component.html',
    styleUrls: ['./note-edit.component.css']
})
export class NoteEditComponent implements OnInit {
    note: Note | null = null;
    noteId: string | null = null;
    noteForm!: FormGroup;
    updatingNote = false;
    updatingMode = false;
    error: string = '';

    constructor(private route: ActivatedRoute, private notesService: NotesService,
        private authService: AuthService, private router: Router) { }

    ngOnInit(): void {
        const token = localStorage.getItem('token');

        if (token) {
            this.authService.token.next(token);
        }

        this.route.params
            .subscribe(
                (params: Params) => {
                    this.noteId = params['noteId'];

                    if (this.noteId) {
                        this.updatingMode = true;
                        this.notesService.getUserNoteById(this.noteId!!).subscribe(note => {
                            this.note = note;
                
                            this.noteForm = new FormGroup({
                                'text': new FormControl(null, [Validators.required])
                            })
    
                            this.noteForm.setValue({
                                'text': this.note!!.text
                            })
                        });
                    } else {
                        this.updatingMode = false;
                        this.noteForm = new FormGroup({
                            'text': new FormControl(null, [Validators.required])
                        })
                    }
                }
            );
    }

    onSubmit() {
        this.updatingNote = true;

        let noteObservable: Observable<any>;

        if (this.updatingMode) {
            noteObservable = this.notesService.updateNoteText(this.noteId!!, this.noteForm.get('text')!!.value);
        } else {
            noteObservable = this.notesService.createUserNote(this.noteForm.get('text')!!.value);
        }

        noteObservable.subscribe({
            next: () => {
                this.updatingNote = false;
                this.router.navigate(['/']);
            },
            error: (errorMessage: string) => {
                this.updatingNote = false;
                this.error = errorMessage;
            }
        })
    }
}
