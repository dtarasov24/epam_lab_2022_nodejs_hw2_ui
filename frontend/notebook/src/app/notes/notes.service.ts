import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { catchError, map, tap } from 'rxjs/operators';
import { Note } from './note.model';

export interface GetNotesResponse {
    offset: string;
    limit: string;
    count: string;
    notes: Note[];
}

export interface GetNoteByIdResponse {
    note: Note
}

@Injectable({
    providedIn: 'root'
})
export class NotesService {

    constructor(private http: HttpClient) { }

    getUserNotes(offset: number) {
        return this.http.get<GetNotesResponse>(`http://localhost:8080/api/notes`, {
            params: new HttpParams().append('offset', offset)
        });
    }

    getUserNoteById(noteId: string) {
        return this.http.get<GetNoteByIdResponse>(`http://localhost:8080/api/notes/${noteId}`)
            .pipe(
                map(note => note.note)
            );
    }

    toggleNoteCompleteStatus(noteId: string) {
        return this.http.patch(`http://localhost:8080/api/notes/${noteId}`, {});
    }

    updateNoteText(noteId: string, text: string) {
        return this.http.put(`http://localhost:8080/api/notes/${noteId}`, { text })
            .pipe(
                catchError(errorRes => throwError(() => new Error(errorRes?.error?.message)))
            );
    }

    deleteNote(noteId: string) {
        return this.http.delete(`http://localhost:8080/api/notes/${noteId}`);
    }

    createUserNote(text: string) {
        return this.http.post(`http://localhost:8080/api/notes`, { text })
            .pipe(
                catchError(errorRes => throwError(() => new Error(errorRes?.error?.message)))
            );
    }
}
