import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Note } from '../note.model';
import { NotesService } from '../notes.service';

@Component({
    selector: 'app-note-item',
    templateUrl: './note-item.component.html',
    styleUrls: ['./note-item.component.css']
})
export class NoteItemComponent implements OnInit {
    @Input('note') note: Note | undefined;
    @Output() noteChanged = new EventEmitter<string>();
    @Output() noteDeleted = new EventEmitter<void>();
    
    constructor(private notesService: NotesService) { }

    ngOnInit() {}

    toggleNoteCompleteStatus() {
        this.notesService.toggleNoteCompleteStatus(this.note?._id!!).subscribe(_ => {
            this.noteChanged.emit(this.note?._id);
        });
    }

    deleteNote() {
        this.notesService.deleteNote(this.note?._id!!).subscribe(_ => {
            this.noteDeleted.emit();
        });
    }
}
