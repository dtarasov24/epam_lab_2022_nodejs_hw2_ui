import bcrypt from 'bcryptjs';
import mongoose from 'mongoose';

import HttpError from '../models/http-error.js';
import User from '../models/user.js';
import Note from '../models/note.js';

export const getProfileInfo = async (req, res, next) => {
    let user;
    try {
        user = await User.findById(req.userData.userId);
    } catch (err) {
        return next(new HttpError('Fetching a user from db failed', 500));
    }

    if (!user) {
        return next('User doesn`t exist', 500);
    }

    res.status(200).json({
        user: {
            _id: user._id,
            username: user.username,
            createdDate: user.createdDate
        }
    });
}

export const changeProfilePassword = async (req, res, next) => {
    const { oldPassword, newPassword } = req.body;

    let user;
    try {
        user = await User.findById(req.userData.userId);
    } catch (err) {
        return next(new HttpError('Fetching a user from db failed', 500));
    }

    if (!user) {
        return next('User doesn`t exist', 500);
    }

    let isValidPassword = false;
    try {
        isValidPassword = await bcrypt.compare(oldPassword, user.password);
    } catch (err) {
        return next(new HttpError('Could not change the password, please try again later', 500));
    }

    if (!isValidPassword) {
        return next(new HttpError('Invalid old password', 400));
    }

    let hashedPassword;
    try {
        hashedPassword = await bcrypt.hash(newPassword, 10);
    } catch (err) {
        return next(new HttpError('Could not change the password, please try again later', 500));
    }

    user.password = hashedPassword;

    try {
        await user.save();
    } catch (error) {
        return next(new HttpError('Something went wrong. Could not update a user password', 500));
    }

    res.status(200).json({ message: 'Success' });
}

export const deleteProfile = async (req, res, next) => {

    try {
        const sess = await mongoose.startSession();
        sess.startTransaction();
        await User.findByIdAndRemove(req.userData.userId).session(sess);
        await Note.remove({ User: req.userData.userId }).session(sess);
        await sess.commitTransaction();
    } catch (err) {
        return next(new HttpError('Could not delete a user, please try again later', 500));
    }

    res.status(200).json({ message: 'Success' });
}   