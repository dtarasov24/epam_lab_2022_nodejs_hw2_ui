import HttpError from '../models/http-error.js';
import User from '../models/user.js';
import Note from '../models/note.js';

export const getUserNotes = async (req, res, next) => {
    const offset = req.query.offset || 0;
    const limit = req.query.limit || 3;

    let userNotes;
    let count;
    try {
        count = await Note.find({ userId: req.userData.userId }).count();
        userNotes = await Note.find({ userId: req.userData.userId }).skip(offset).limit(limit).select('-__v');
    } catch (err) {
        return next(new HttpError('Fetching user notes from db failed', 500));
    }

    res.status(200).json({
        offset,
        limit,
        count,
        notes: userNotes
    });
}

export const addUserNotes = async (req, res, next) => {
    const { text } = req.body;

    const createdNote = new Note({
        userId: req.userData.userId,
        completed: false,
        text,
        createdDate: new Date().toISOString()
    });

    let user;
    try {
        user = await User.findById(req.userData.userId);
    } catch (error) {
        return next(new HttpError('Create a user note failed, please try again', 500));
    }

    if (!user) {
        return next(new HttpError('Could not find a user for the provided id', 404));
    }

    try {
        await createdNote.save();
    } catch (error) {
        return next(new HttpError('Create a user note failed, please try again', 500));
    }

    res.status(200).json({ message: 'Success' });
}

export const getUserNoteById = async (req, res, next) => {
    const noteId = req.params.noteId;

    let note;
    try {
        note = await Note.findById(noteId).select('-__v');
    } catch (err) {
        return next(new HttpError('Could not get a note, try again later', 500));
    }

    if (!note) {
        return next(new HttpError('Note with the provided id doesn`t exist', 404));
    }

    res.status(200).json({
        note
    })
}

export const updateUserNoteById = async (req, res, next) => {
    const noteId = req.params.noteId;
    const { text } = req.body;

    if (!text) {
        return next(new HttpError('Please, provide the updated note text', 400));
    }

    try {
        await Note.findByIdAndUpdate(noteId, { text });
    } catch (err) {
        return next(new HttpError('Could not get a note, try again later', 500));
    }

    res.status(200).json({ message: 'Success' });
}

export const toggleCompletedForUserNoteById = async (req, res, next) => {
    const noteId = req.params.noteId;

    let note;
    try {
        note = await Note.findById(noteId).select('-__v');
    } catch (err) {
        return next(new HttpError('Could not get a note, try again later', 500));
    }

    note.completed = !note.completed;

    try {
        await note.save();
    } catch (err) {
        return next(new HttpError('Could not update a note', 500));
    }

    res.status(200).json({ message: 'Success' });
}

export const deleteUserNoteById = async (req, res, next) => {
    const noteId = req.params.noteId;

    try {
        await Note.findByIdAndRemove(noteId);
    } catch (err) {
        return next(new HttpError('Could not delete a user note', 500));
    }

    res.status(200).json({ message: 'Success' });
}